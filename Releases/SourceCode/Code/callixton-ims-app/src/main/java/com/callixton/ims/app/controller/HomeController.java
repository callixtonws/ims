package com.callixton.ims.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.callixton.ims.app.service.UserService;
import com.callixton.ims.data.entity.User;

@Controller
public class HomeController {
	
	@Autowired
	private UserService userService;

	@GetMapping(value = "/user/home")
	public ModelAndView userHome() {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByUserName(auth.getName());
		modelAndView.addObject("fullName", user.getFullName());
		modelAndView.setViewName("home");
		return modelAndView;
	}
}
