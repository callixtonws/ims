package com.callixton.ims.app.service;

import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.callixton.ims.data.entity.User;
import com.callixton.ims.data.repository.UserRepository;

@Service
public class UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

	/** The user repository. */
	@Autowired
	private UserRepository userRepository;

	/**
	 * Creates the.
	 *
	 * @param user the user
	 * @return the user
	 */
	@Transactional
	public User create(User user) {
		return userRepository.save(user);
	}

	/**
	 * Update.
	 *
	 * @param user the user
	 * @return the user
	 */
	@Transactional
	public User update(User user) {

		LOGGER.debug("Saving user {}", user);

		return userRepository.save(user);
	}

	/**
	 * Delete.
	 *
	 * @param id the id
	 */
	public void delete(Long id) {
		User user = userRepository.getById(id);
		user.setVoided(Boolean.TRUE);
		userRepository.save(user);
	}

	/**
	 * Gets the by id.
	 *
	 * @param id the id
	 * @return the by id
	 */
	public User getById(Long id) {
		return userRepository.getById(id);
	}

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	public List<User> getAll() {
		return userRepository.getAll();
	}
	
	public User findUserByUserName(String username) {
		return userRepository.getByUsername(username);
	}

	/**
	 * Generate password.
	 *
	 * @return the string
	 */
	public String generatePassword() {
		List<CharacterRule> rules = Arrays.asList(
				// at least one upper-case character
				new CharacterRule(EnglishCharacterData.UpperCase, 1),

				// at least one lower-case character
				new CharacterRule(EnglishCharacterData.LowerCase, 1),

				// at least one digit character
				new CharacterRule(EnglishCharacterData.Digit, 1),

				// at least one special character
				new CharacterRule(EnglishCharacterData.Special, 1));

		PasswordGenerator generator = new PasswordGenerator();

		// Generated password is 12 characters long, which complies with policy
		String password = generator.generatePassword(12, rules);
		return password;
	}

}
