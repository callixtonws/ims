/*
 * Copyright (c) Callixton Private Limited 2021. All rights reserved. <br><br> 
 *
 * @Auther Callixton Private Limited
 * @email info@callixton.com
 */
package com.callixton.ims.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.callixton.ims.data.entity.Privilege;

/**
 * The Interface PrivilegeRepository.
 */
@Repository
public interface PrivilegeRepository extends JpaRepository<Privilege, Long> {

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	@Query(value = "SELECT a FROM Privilege a WHERE a.voided = 0")
	List<Privilege> getAll();
}
