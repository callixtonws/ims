/*
 * Copyright (c) Callixton Private Limited 2021. All rights reserved. <br><br> 
 *
 * @Auther Callixton Private Limited
 * @email info@callixton.com
 */
package com.callixton.ims.data.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * The Class Role.
 */
@Entity
@Table(name = "u_role")
@Setter(value = AccessLevel.PUBLIC)
@Getter(value = AccessLevel.PUBLIC)
public class Role extends BaseEntity {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5575686329906990336L;

	/** The name. */
	@NotNull
	@Column(name = "name", unique = true)
	private String name;

	/** The description. */
	@Column(name = "description")
	private String description;

	/** The privileges. */
	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE })
	@JoinTable(name = "u_role_privilege_mapping", joinColumns = {
			@JoinColumn(name = "role_id") }, inverseJoinColumns = { @JoinColumn(name = "privilege_id") })
	private Set<Privilege> privileges = new HashSet<>();

}
