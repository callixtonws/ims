/*
 * Copyright (c) Callixton Private Limited 2021. All rights reserved. <br><br> 
 *
 * @Auther Callixton Private Limited
 * @email info@callixton.com
 */
package com.callixton.ims.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * The Class Privilege.
 */
@Entity
@Table(name = "u_privilege")
@Setter(value = AccessLevel.PUBLIC)
@Getter(value = AccessLevel.PUBLIC)
public class Privilege extends BaseEntity {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2823627359274790511L;

	/** The code. */
	@NotNull
	@Column(name = "code", unique = true)
	private String code;

	/** The description. */
	@Column(name = "description", unique = true)
	private String description;

}
