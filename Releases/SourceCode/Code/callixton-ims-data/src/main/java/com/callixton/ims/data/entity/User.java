package com.callixton.ims.data.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * The Class BaseUser.
 */
@Entity
@Table(name = "u_user")
@Setter(value = AccessLevel.PUBLIC)
@Getter(value = AccessLevel.PUBLIC)
public class User extends BaseEntity implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4702108842959161268L;

	/** The username. */
	@NotNull(message = "Username cannot be null")
	@NotEmpty(message = "Username cannot be empty")
	@Column(name = "username", unique = true)
	private String username;

	/** The full name. */
	@NotNull(message = "Full name cannot be null")
	@NotEmpty(message = "Full name cannot be empty")
	@Column(name = "full_name")
	private String fullName;

	/** The password. */
	@Column(name = "password")
	private String password;

	/** The password reset at. */
	@Column(name = "password_reset_at")
	private Date passwordResetAt;

	/** The email. */
	@Email
	@NotNull(message = "Email cannot be null")
	@NotEmpty(message = "Email cannot be empty")
	@Column(name = "email", unique = true)
	private String email;

	/** The registration completed. */
	@NotNull
	@Column(name = "registration_completed", unique = true)
	private Boolean registrationCompleted = Boolean.FALSE;

	/** The roles. */
	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE })
	@JoinTable(name = "u_user_role_mapping", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = {
			@JoinColumn(name = "role_id") })
	private Set<Role> roles = new HashSet<>();

}