/*
 * Copyright (c) Callixton Private Limited 2021. All rights reserved. <br><br> 
 *
 * @Auther Callixton Private Limited
 * @email info@callixton.com
 */
package com.callixton.ims.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * The Class WebApplication.
 */
@SpringBootApplication
@EntityScan(basePackages = { "com.callixton.ims.data.entity" })
@EnableJpaRepositories(basePackages = { "com.callixton.ims.data.repository" })
public class WebApplication {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(WebApplication.class, args);
	}

}
