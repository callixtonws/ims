/*
 * Copyright (c) Callixton Private Limited 2020. All rights reserved. <br><br> 
 *
 * @Auther Callixton Private Limited
 * @email info@callixton.com
 */
package com.callixton.ims.app.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.callixton.ims.data.entity.Privilege;
import com.callixton.ims.data.entity.Role;
import com.callixton.ims.data.entity.User;
import com.callixton.ims.data.repository.UserRepository;

/**
 * The Class AppUserDetailsServiceImpl.
 */
@Service
public class AppUserDetailsService implements UserDetailsService {

	/** The user repository. */
	@Autowired
	private UserRepository userRepository;

	/**
	 * Load user by username.
	 *
	 * @param username the username
	 * @return the user details
	 * @throws UsernameNotFoundException the username not found exception
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.getByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		
        List<GrantedAuthority> authorities = getUserAuthority(user.getRoles());

		return buildUserForAuthentication(user, authorities);
	}
	
	 private List<GrantedAuthority> getUserAuthority(Set<Role> userRoles) {
	        Set<GrantedAuthority> roles = new HashSet<GrantedAuthority>();
	        for (Role role : userRoles) {
	        	Set<Privilege> privileges = role.getPrivileges();
	        	 for (Privilege privilege : privileges) {
	        		 roles.add(new SimpleGrantedAuthority(privilege.getCode()));
	        	 }
	        }
	        List<GrantedAuthority> grantedAuthorities = new ArrayList<>(roles);
	        return grantedAuthorities;
	    }

	    private UserDetails buildUserForAuthentication(User user, List<GrantedAuthority> authorities) {
	        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
	                user.isEnabled(), true, true, true, authorities);
	    }
}
